import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { of } from 'rxjs/observable/of';

import {
    debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {
    heroes$: Observable<Hero[]>;
    private searchTerms = new Subject<string>();
  constructor(private HeroService: HeroService) { }

  search(term: string): void {
      this.searchTerms.next(term);
  }

  ngOnInit(): void {
      this.heroes$ = this.searchTerms.pipe(
          // wait before launching search for term
          debounceTime(300),
          // don't launch search unless changed
          distinctUntilChanged(),
          // switch to new observable when launching search
          switchMap((term: string) => this.HeroService.searchHeroes(term)),
      );
  }

}
